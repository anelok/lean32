/*
 * fw/lean32/lean32.h - Lean32 API
 *
 * Written 2017 by Werner Almesberger
 * Copyright 2017 Werner Almesberger
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 */


#ifndef LEAN32_H
#define	LEAN32_H

#include <stdbool.h>
#include <stdint.h>


enum lean32_return_codes {
	LEAN32_SUCCESS	= 0,
	LEAN32_IO	= -1,   /* I/O error */
	LEAN32_NOT_FOUND= -2,   /* file not found */
	LEAN32_INVALID	= -3,	/* invalid value */
};

#define	SECTOR_SIZE	512


struct lean32_disk_ops {
	bool (*read)(void *ctx, void *sec, uint32_t num);
	bool (*write)(void *ctx, const void *sec, uint32_t num);
};

/* Applications shouldn't access "struct lean32_fs" fields directly */

struct lean32_fs {
	const struct lean32_disk_ops *ops;
	void *ctx;
	uint32_t fat_start;		/* first sector of first FAT */
	uint32_t cluster_base;		/* first sector of "cluster 0" */
	uint8_t secs_per_clus;		/* cluster size */
	uint8_t fats;			/* number of FATs */
	uint32_t secs_per_fat;		/* sectors per FAT */
	uint32_t root_cluster;		/* first cluster of root directory */
};

/* Applications shouldn't access "struct lean32_file" fields directly */

struct lean32_file {
	const struct lean32_fs *fs;
	uint32_t size;			/* file size in bytes */
	uint32_t start;			/* first cluster */
	uint32_t cluster_in_file;	/* current cluster in file */
	uint32_t cluster_on_disk;	/* current cluster on disk */
	uint32_t pos;			/* byte in file */
	bool wb_error;			/* writeback failed */
};

/* Applications shouldn't access "struct lean32_dir" fields directly */

struct lean32_dir {
	struct lean32_file file;	/* "size" and "start" are not used */
};

struct lean32_dir_entry {
	const char *name; /* name (8) and extension (3) */
	uint32_t start;	/* first cluster of file */
	uint32_t size;	/* file size in bytes */
};


int8_t lean32_mount(struct lean32_fs *fs, const struct lean32_disk_ops *ops,
    void *ctx);

void lean32_rewind_dir(const struct lean32_fs *fs, struct lean32_dir *dir);
int8_t lean32_next_entry(struct lean32_dir *dir,
    struct lean32_dir_entry *entry);

bool lean32_match(const struct lean32_dir_entry *entry, const char *name);
int8_t lean32_lookup(const struct lean32_fs *fs, struct lean32_dir_entry *entry,
    const char *name);

bool lean32_open(const struct lean32_fs *fs, struct lean32_file *file,
    const struct lean32_dir_entry *entry);
int8_t lean32_close(struct lean32_file *file);

void lean32_seek(struct lean32_file *file, uint32_t offset);
uint32_t lean32_tell(struct lean32_file *file);

int16_t lean32_read(struct lean32_file *file, const void **buf, uint16_t size);
int16_t lean32_write(struct lean32_file *file, const void *buf, uint16_t size);

#endif /* !LEAN32_H */
