/*
 * fw/lean32/fat32.h - FAT32 filesystem on-disk structures
 *
 * Written 2017 by Werner Almesberger
 * Copyright 2017 Werner Almesberger
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 */


#ifndef FAT32_H
#define	FAT32_H

#include <stdint.h>


/*
 * Loosely based on Linux include/linux/msdos_fs.h
 * from http://ftp.icm.edu.pl/packages/linux-tsx-11/ALPHA/dosfs/dosfs.12.tar.gz
 * and https://en.wikipedia.org/wiki/Design_of_the_FAT_file_system
 */

/*
 * We use arrays for unaligned fields.
 */

struct fat_boot_sector {
	uint8_t jump[3];		/* boot strap short or near jump */
        uint8_t system_id[8];		/* name */
        uint8_t sector_size[2];		/* bytes per logical sector */

	/* DOS BPB */

	uint8_t secs_per_clus;		/* sectors per cluster */
	uint16_t reserved;		/* reserved sectors */
	uint8_t fats;			/* number of FATs */
	uint8_t dir_entries[2];		/* root dir entries (0 on FAT32) */
	uint8_t sectors[2];		/* number of sectors */
	uint8_t media;			/* media code */
	uint16_t fat_length;		/* sectors per FAT (0 on FAT32) */
	uint16_t secs_track;		/* sectors per track */
	uint16_t heads;			/* number of heads */
	uint32_t hidden;		/* hidden sectors (unused) */
	uint32_t total_secs;		/* number of secs if sectors[] == 0 */

	/* Fields specific to FAT32 */

	uint32_t fat_length_32;		/* sectors per FAT */
	uint16_t flags;
	uint16_t version;		/* file system version */
	uint32_t root_cluster;		/* first cluster in root directory */
	uint16_t fs_info_sec;		/* file system info sector */
	uint16_t backup_boot;		/* backup boot sector */
	uint16_t reserved2[6];		/* unused */

	/* Extended BIOS Parameter Block */

	uint8_t driver_number;		/* physical drive number */
	uint8_t state;			/* mount state */
	uint8_t signature;		/* extended boot signature */
	uint8_t vol_id[4];		/* volume ID */
	uint8_t vol_label[11];		/* volume label */
	uint8_t fs_type[8];		/* file system type */
};

struct fat_dir_entry {
	char name[11];			/* name and extension */
	uint8_t attr;			/* file attributes */
	uint8_t unused[8];
	uint16_t start_cluster_high;	/* first cluster, upper 16 bits */
	uint16_t time;			/* modification time */
	uint16_t date;			/* modification date */
	uint16_t start_cluster_low;	/* first cluster, lower 16 bits */
	uint32_t size;			/* file size in bytes */
};

#endif /* !FAT32_H */
