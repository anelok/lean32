/*
 * fw/lean32/lean32.c - Lean FAT32 system
 *
 * Written 2017 by Werner Almesberger
 * Copyright 2017 Werner Almesberger
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 */

/*
 * @@@ We assume we're on little-endian.
 * note: we use "const" a lot. May have to drop some of that, especially on
 * "fs", in case we add caching at that level, e.g., for cluster allocation.
 *
 * To do:
 * - writes
 * - subdirectories ?
 * - file creation ?
 * - deletion ?
 * - renames ?
 */


#include <stdbool.h>
#include <stdint.h>
#include <ctype.h>
#include <string.h>

#include "fat32.h"
#include "lean32.h"


#define	FIRST_CLUSTER	2
#define	FAT32_MASK	0x0fffffff      /* upper four bits contain flags */


static uint8_t buffer[SECTOR_SIZE];	/* @@@ assume alignment >= 4 bytes */
static uint32_t curr_sec;		/* sector in buffer, 0 if invalid */
/*
 * "dirty" is non-NULL if the buffer contains unwritten data. If an error
 * occurs during writeback, the error is attributed to the file "dirty" points
 * to. This means that write operation that have no associated open file have
 * to write back immediately.
 */
static struct lean32_file *dirty = NULL;


/* ----- Helper functions -------------------------------------------------- */


static void write_back(const struct lean32_fs *fs)
{
	if (dirty) {
		if (!fs->ops->write(fs->ctx, buffer, curr_sec))
			dirty->wb_error = 1;
	}
	dirty = 0;
}


static bool get_sector(const struct lean32_fs *fs, uint32_t sec)
{
	if (curr_sec == sec)
		return 1;
	write_back(fs);
	if (fs->ops->read(fs->ctx, buffer, sec)) {
		curr_sec = sec;
		return 1;
	} else {
		curr_sec = 0;
		return 0;
	}
}


/* ----- FAT operations ---------------------------------------------------- */


static int8_t next_cluster(struct lean32_file *file)
{
	const struct lean32_fs *fs = file->fs;
	uint32_t fat_off, fat_sec;
	uint32_t next;

	/* we always read from the first FAT */
	fat_off = file->cluster_on_disk * 4;
	fat_sec = fs->fat_start + fat_off / SECTOR_SIZE;
	if (!get_sector(fs, fat_sec))
		return LEAN32_IO;
	next = *(const uint32_t *) (buffer + fat_off % SECTOR_SIZE);
	next &= FAT32_MASK;
	if (next < FIRST_CLUSTER)
		return 0;
	if (next >= 0x0ffffff0)
		return 0;
	file->cluster_on_disk = next;
	file->cluster_in_file++;
	return 1;
}


static int8_t go_to_cluster(struct lean32_file *file, uint32_t cluster)
{
	int8_t res;

	if (cluster == file->cluster_in_file)
		return 1;
	file->cluster_on_disk = file->start;
	file->cluster_in_file = 0;
	while (file->cluster_in_file != cluster) {
		res = next_cluster(file);
		if (res <= 0)
			return res;
	}
	return 1;
}


static int8_t get_sector_from_file(struct lean32_file *file)
{
	const struct lean32_fs *fs = file->fs;
	uint32_t cluster, sec;
	int8_t res;

	cluster = (file->pos / SECTOR_SIZE) * fs->secs_per_clus;
	res = go_to_cluster(file, cluster);
	if (res <= 0)
		return res;

	sec = fs->cluster_base
	    + file->cluster_on_disk * fs->secs_per_clus
	    + file->pos / SECTOR_SIZE;
	return get_sector(fs, sec) ? 1 : LEAN32_IO;
}


/* ----- File system initialization ---------------------------------------- */


int8_t lean32_mount(struct lean32_fs *fs, const struct lean32_disk_ops *ops,
    void *ctx)
{
	const struct fat_boot_sector *boot = (const void *) buffer;

	fs->ops = ops;
	fs->ctx = ctx;
	if (!ops->read(ctx, buffer, 0))
		return LEAN32_IO;
	fs->secs_per_clus = boot->secs_per_clus;
	fs->fats = boot->fats;
	fs->secs_per_fat = boot->fat_length_32;
	fs->fat_start = boot->reserved;
	fs->cluster_base = boot->reserved
	    + boot->fats * boot->fat_length_32
	    - FIRST_CLUSTER * boot->secs_per_clus;
	fs->root_cluster = boot->root_cluster;
	/* could return LEAN32_INVALID */
	return 0;
}


/* ----- Directory read ---------------------------------------------------- */


void lean32_rewind_dir(const struct lean32_fs *fs, struct lean32_dir *dir)
{
	dir->file.fs = fs;
	dir->file.start = dir->file.cluster_on_disk = fs->root_cluster;
	dir->file.cluster_in_file = 0;
	dir->file.pos = 0;
}


int8_t lean32_next_entry(struct lean32_dir *dir,
    struct lean32_dir_entry *entry)
{
	const struct fat_dir_entry *de;
	int8_t res;

	res = get_sector_from_file(&dir->file);
	if (res <= 0)
		return res;

	de = (const struct fat_dir_entry *)
	    (buffer + dir->file.pos % SECTOR_SIZE);
	entry->name = de->name;
	entry->start = de->start_cluster_low | de->start_cluster_high << 16;
	entry->size = de->size;

	dir->file.pos += sizeof(struct fat_dir_entry);
	return 1;
}


/* ----- File lookup ------------------------------------------------------- */


static inline bool match_char(char a_fs, char b)
{
	return (islower(b) && a_fs == toupper(b)) || a_fs == b;
}


bool lean32_match(const struct lean32_dir_entry *entry, const char *name)
{
	const char *s = entry->name;
	uint8_t i;

	for (i = 0; i != 8; i++) {
		if (!*name || *name == '.') {
			if (*s != ' ')
				return 0;
		} else {
			if (!match_char(*s, *name))
				return 0;
			name++;
		}
		s++;
	}

	if (!*name)
		return !memcmp(s, "   ", 3);
	if (*name != '.')
		return 0;
	name++;

	for (i = 0; i != 3; i++) {
		if (*name) {
			if (!match_char(*s, *name))
				return 0;
			name++;
		} else {
			if (*s != ' ')
				return 0;
		}
		s++;
	}
	return !*name;
}


int8_t lean32_lookup(const struct lean32_fs *fs, struct lean32_dir_entry *entry,
    const char *name)
{
	struct lean32_dir dir;
	int8_t res;

	lean32_rewind_dir(fs, &dir);
	while (1) {
		res = lean32_next_entry(&dir, entry);
		if (!res)
			return LEAN32_NOT_FOUND;
		if (res < 0)
			return res;
		if (lean32_match(entry, name))
			return 0;
	}
}


/* ----- File access ------------------------------------------------------- */


bool lean32_open(const struct lean32_fs *fs, struct lean32_file *file,
    const struct lean32_dir_entry *entry)
{
	file->fs = fs;
	file->start = entry->start;
	file->size = entry->size;
	file->cluster_on_disk = file->start;
	file->cluster_in_file = 0;
	file->pos = 0;
	file->wb_error = 0;
	return 1;
}


int8_t lean32_close(struct lean32_file *file)
{
	write_back(file->fs);
	return file->wb_error ? LEAN32_IO : LEAN32_SUCCESS;
}


/* ----- Seeking ----------------------------------------------------------- */


/* @@@ develop this into an API that also returns the cluster */

void lean32_seek(struct lean32_file *file, uint32_t offset)
{
	file->pos = offset;
}


uint32_t lean32_tell(struct lean32_file *file)
{
	return file->pos;
}


/* ----- Data I/O ---------------------------------------------------------- */


int16_t lean32_read(struct lean32_file *file, const void **buf, uint16_t size)
{
	uint16_t offset;
	int8_t res;

	if (size >= 0x8000)
		return LEAN32_INVALID;
	if (file->pos >= file->size)
		return 0;
	res = get_sector_from_file(file);
	if (res <= 0)
		return res;

	offset = file->pos % SECTOR_SIZE;
	if (offset + size > SECTOR_SIZE)
		size = SECTOR_SIZE - offset;
	if (file->pos + size > file->size)
		size = file->size - file->pos;
	file->pos += size;

	*buf = buffer + offset;
	return size;
}


int16_t lean32_write(struct lean32_file *file, const void *buf, uint16_t size)
{
	if (size >= 0x8000)
		return LEAN32_INVALID;
	return 0;
}
