Lean32 implements basic operations for FAT32 file systems. It is intended
for RAM-constrained environments, such as low-end 32 bit microcontrollers
found in embedded systems. Lean32 is part of the Anelok project.


LGPLv2.1+
---------

Lean32 is distributed under the GNU LESSER GENERAL PUBLIC LICENSE,
Version 2.1.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

For your convenience, a copy of the complete LGPL has been included in
the file COPYING.LGPLv21.


Limitations
-----------

Lean32 does not support the following features and operations:
- VFAT-style file names (i.e., anything but 8+3 unicase)
- subdirectories
- file creation
- file deletion
- renaming
