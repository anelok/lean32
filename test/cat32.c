#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>

#include "lean32.h"


/* ----- "Disk" I/O -------------------------------------------------------- */


static bool read_sector(void *ctx, void *sec, uint32_t num)
{
	const int *fd = ctx;
	ssize_t got;

	if (lseek(*fd, num * SECTOR_SIZE, SEEK_SET) == (off_t) -1) {
		perror("lseek");
		return 0;
	}
	got = read(*fd, sec, SECTOR_SIZE);
	if (got < 0)
		perror("read");
	return got == SECTOR_SIZE;
}


static bool write_sector(void *ctx, const void *sec, uint32_t num)
{
	abort();
}


static const struct lean32_disk_ops disk_ops = {
	.read	= read_sector,
	.write	= write_sector,
};


/* ----- File lookup ------------------------------------------------------- */


static bool open_file(const struct lean32_fs *fs, struct lean32_file *file,
    const char *name)
{
	struct lean32_dir_entry entry;
	int32_t res;

	res = lean32_lookup(fs, &entry, name);
	if (res < 0) {
		fprintf(stderr, "%s: lean32_lookup failed (%d)\n", name, res);
		return 0;
	}
	if (!lean32_open(fs, file, &entry)) {
		fprintf(stderr, "%s: lean32_open failed\n", name);
		return 0;
	}
	return 1;
}


/* ----- File I/O ---------------------------------------------------------- */


static bool cat_file(struct lean32_file *file, FILE *out)
{
	int16_t got;
	const void *buf;

	while (1) {
		got = lean32_read(file, &buf, 1000);
		if (!got)
			return 1;
		if (got < 0)
			return 0;
		if (fwrite(buf, 1, got, out) != (size_t) got) {
			perror("fwrite");
			return 0;
		}
	}
}


/* ----- Invocation -------------------------------------------------------- */



static void usage(const char *name)
{
	fprintf(stderr, "usage: %s device_or_file file_name\n", name);
	exit(1);
}


int main(int argc, char **argv)
{
	struct lean32_fs fs;
	struct lean32_file file;
	int fd;
	int8_t res;

	if (argc != 3)
		usage(*argv);

	fd = open(argv[1], O_RDONLY);
	if (fd < 0) {
		perror(argv[1]);
		return 1;
	}

	res = !lean32_mount(&fs, &disk_ops, &fd);
	if (res < 0) {
		fprintf(stderr, "lean32_mount failed (%d)\n", res);
		return 1;
	}

	if (!open_file(&fs, &file, argv[2]))
		return 1;
	if (!cat_file(&file, stdout))
		return 1;
	return 0;
}
